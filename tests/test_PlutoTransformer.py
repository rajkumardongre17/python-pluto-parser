"""Test the PlutoTransformer and its methods for correct processing."""
from textwrap import dedent
from unittest.mock import patch

import pytest

from units import ureg
from pluto_parser.transformer import PlutoTransformer
from pluto_parser import parser, pluto_parse


def test_parser_instantiation():
    """Test if the standard PLUTO parser instantiates and is a Lark object."""
    p = parser.PlutoParser
    assert isinstance(p, parser.Lark)


@pytest.mark.parametrize("val", ["-5 V/m", "4 V/m", "1s", "10 m/s", "5 deg/h"])
def test_int_constant_unit(pluto_parser, val):
    p = pluto_parser(start="integer_constant")
    tree = p.parse(val)
    const = PlutoTransformer().transform(tree)
    assert eval(const) == ureg(val)


def test_degree_unit(pluto_parser):
    """The symbol for hour "h" gets correctly recognised."""
    p = pluto_parser(start="integer_constant")
    tree = p.parse("300 deg/h")
    res = eval(PlutoTransformer().transform(tree))
    assert res.m_as("deg/min") == 5


@pytest.mark.parametrize("val", ["-5", "4"])
def test_int_constant_nounit(pluto_parser, val):
    p = pluto_parser(start="integer_constant")
    tree = p.parse(val)
    const = PlutoTransformer().transform(tree)
    assert eval(const) == int(val)


def test_int_constant_hex(pluto_parser):
    p = pluto_parser(start="integer_constant")
    tree = p.parse("0xFF")
    const = PlutoTransformer().transform(tree)
    assert eval(const) == 255


def test_string_constant(pluto_parser):
    p = pluto_parser(start="constant")
    tree = p.parse('"ab AB"')
    const = PlutoTransformer().transform(tree)
    assert const == '"ab AB"'


@pytest.mark.skip
@pytest.mark.parametrize(
    "input, result",
    [
        ("1s", ureg("1s")),
        # ('29h', ureg('1d') + ureg('5h')),
    ],
)
def test_relative_time_constant(pluto_parser, input, result):
    """Check for issue 34."""
    p = pluto_parser(start="relative_time_constant")
    tree = p.parse(input)
    val = PlutoTransformer().transform(tree)
    print(tree.pretty())
    assert (
        eval(val) == result
    )  # To see if created uregs are valid, evalute val


@pytest.mark.skip
@pytest.mark.parametrize(
    "input, result",
    [
        ("1s", "ureg('1s')"),
        (
            "2001-08-18T21:07:43.137468Z",
            "datetime(2001, 8, 18, 21, 7, 43, 137468)",
        ),
    ],
)
def test_constant(pluto_parser, input, result):
    p = pluto_parser(start="constant")
    tree = p.parse(input)
    val = PlutoTransformer().transform(tree)
    print(tree.pretty())
    assert val == result


@pytest.mark.skip
def test_pluto_to_python():
    pluto_source = dedent(
        """\
    procedure
        main
            log "Hello World!";
        end main
    end procedure
    """
    )
    # mock out the start method to generate a simple return
    # we just want to ensure that the transformer generates the expected
    # result
    with patch.object(
        PlutoTransformer, "procedure_definition", return_value="abc"
    ):
        py_source = pluto_parse(pluto_source)
    py_expected = "abc"
    assert py_source == py_expected


@pytest.mark.parametrize(
    "input, result", [("TRUE", "True"), ("FALSE", "False")]
)
def test_boolean_constant(pluto_parser, input, result):
    p = pluto_parser(start="boolean_constant")
    tree = p.parse(input)
    val = PlutoTransformer().transform(tree)
    assert val == result


@pytest.mark.parametrize(
    "input, result",
    [
        ("AND", "and"),
        ("OR", "or"),
        ("XOR", "xor"),
    ],
)
def test_boolean_operator(pluto_parser, input, result):
    p = pluto_parser(start="boolean_operator")
    tree = p.parse(input)
    val = PlutoTransformer().transform(tree)
    assert val == result


@pytest.mark.skip
@pytest.mark.parametrize(
    "input, result",
    [
        ("5 < 4", "ureg('5') < ureg('4')"),
        ("5 < 4 AND 3 < 2", "ureg('5') < ureg('4') and ureg('3') < ureg('2')"),
    ],
)
def test_expression(pluto_parser, input, result):
    p = pluto_parser(start="expression")
    tree = p.parse(input)
    val = PlutoTransformer().transform(tree)
    assert val == result


@pytest.mark.skip
@pytest.mark.parametrize(
    "input, result",
    [
        (
            "preconditions wait for 1s then wait for 1s end preconditions",
            "def preconditions(self):\n    self.wait_for_relative_time(ureg('1s'))\n    self.wait_for_relative_time(ureg('1s'))\n",
        )
    ],
)
def test_preconditions_body(pluto_parser, input, result):
    p = pluto_parser(start="preconditions_body")
    tree = p.parse(input)
    val = PlutoTransformer().transform(tree)
    assert val == result


@pytest.mark.skip
@pytest.mark.parametrize(
    "input, result",
    [
        (
            'main log "main body"; end main',
            'def main(self):\n    self.log(str("main body"))\n',
        )
    ],
)
def test_procedure_main_body(pluto_parser, input, result):
    p = pluto_parser(start="procedure_main_body")
    tree = p.parse(input)
    val = PlutoTransformer().transform(tree)
    assert val == result


@pytest.mark.skip
@pytest.mark.parametrize(
    "input, result",
    [
        (
            "confirmation wait until TRUE end confirmation",
            "def confirmation(self):\n    if self.wait_until_expression(lambda: True) is False: return False\n",
        )
    ],
)
def test_confirmation_body(pluto_parser, input, result):
    p = pluto_parser(start="confirmation_body")
    tree = p.parse(input)
    val = PlutoTransformer().transform(tree)
    assert val == result


@pytest.mark.skip
@pytest.mark.parametrize(
    "input, result",
    [
        (
            'initiate and confirm step DummyStep log "initiate and confirm step statement"; end step',
            "stp = Step_DummyStep(self)\nif self.initiate_and_confirm_step(stp) is False: return False\n",
        )
    ],
)
def test_initiate_and_confirm_step_statement(pluto_parser, input, result):
    p = pluto_parser(start="initiate_and_confirm_step_statement")
    tree = p.parse(input)
    val = PlutoTransformer().transform(tree)
    assert val == result


@pytest.mark.skip
@pytest.mark.parametrize(
    "input, result",
    [
        (
            "initiate and confirm activity DummyActivity of DummyElement",
            "act = ActivityCall(self, DummyElement.DummyActivity)\nif self.initiate_and_confirm_activity(act) is False: return False\n",
        )
    ],
)
def test_initiate_and_confirm_activity_statement(pluto_parser, input, result):
    p = pluto_parser(start="initiate_and_confirm_activity_statement")
    tree = p.parse(input)
    val = PlutoTransformer().transform(tree)
    assert val == result


@pytest.mark.skip
@pytest.mark.parametrize(
    "input, result",
    [
        (
            "initiate activity DummyActivity of DummyElement",
            "act = ActivityCall(self, DummyElement.DummyActivity)\nself.initiate_activity(act)\n",
        )
    ],
)
def test_initiate_activity_statement(pluto_parser, input, result):
    p = pluto_parser(start="initiate_activity_statement")
    tree = p.parse(input)
    val = PlutoTransformer().transform(tree)
    assert val == result


@pytest.mark.skip
@pytest.mark.parametrize(
    "input, result",
    [
        (
            'inform user "inform user statement"',
            'self.inform_user(str("inform user statement"))\n',
        )
    ],
)
def test_inform_user_statement(pluto_parser, input, result):
    p = pluto_parser(start="inform_user_statement")
    tree = p.parse(input)
    val = PlutoTransformer().transform(tree)
    assert val == result


@pytest.mark.skip
@pytest.mark.parametrize("input, result", [("1s", "ureg('1s')")])
def test_simple_factor(pluto_parser, input, result):
    p = pluto_parser(start="simple_factor")
    tree = p.parse(input)
    val = PlutoTransformer().transform(tree)
    assert val == result


@pytest.mark.skip
@pytest.mark.parametrize(
    "input, result",
    [
        (
            "declare event TimeoutEvent end declare",
            "def declare(self):\n    evt = Event_TimeoutEvent(self)\n    self.event['TimeoutEvent'] = evt\n",
        )
    ],
)
def test_procedure_declaration_body(pluto_parser, input, result):
    p = pluto_parser(start="procedure_declaration_body")
    tree = p.parse(input)
    val = PlutoTransformer().transform(tree)
    assert val == result


@pytest.mark.skip
@pytest.mark.parametrize(
    "input, result",
    [
        (
            "wait until 2001-08-18T21:07:43.137468Z",
            "self.wait_until_absolute_time(datetime(2001, 8, 18, 21, 7, 43, 137468))\n",
        ),
        ("wait for 1 s", "self.wait_for_relative_time(ureg('1s'))\n"),
        (
            "wait until 10>5",
            "if self.wait_until_expression(lambda: ureg('10') > ureg('5')) is False: return False\n",
        ),
        (
            "wait for event TimeoutEvent",
            "if self.wait_for_event('TimeoutEvent') is False: return False\n",
        ),
        (
            "wait until 5 > 10 timeout 1 s raise event TimeoutEvent",
            "if self.wait_until_expression(lambda: ureg('5') > ureg('10'), timeout=ureg('1s'), raise_event='TimeoutEvent') is False: return False\n",
        ),
    ],
)
def test_wait_statement(pluto_parser, input, result):
    p = pluto_parser(start="wait_statement")
    tree = p.parse(input)
    val = PlutoTransformer().transform(tree)
    assert val == result
